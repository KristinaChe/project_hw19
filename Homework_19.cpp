
#include <iostream>

class Animal
{
public:
    virtual void Voice() const = 0;
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Woof" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Meow" << std::endl;
    }
};

class Sheep : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Baa" << std::endl;
    }
};

int main()
{
    Dog* d = new Dog;
    Cat* c = new Cat;
    Sheep* s = new Sheep;

    Animal* animals[3] = {d, c, s};

    for (Animal* a : animals)
        a->Voice();

    return 0;
}